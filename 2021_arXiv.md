## 2020 August

### [[2008.12769]](https://arxiv.org/pdf/2008.12769.pdf) Prospects for Beyond the Standard Model Physics Searches at the DUNE

### [[2008.12318]](https://arxiv.org/pdf/2008.12318.pdf) Neutrino Counterparts of Fast Radio Bursts


## 2020 September

### [[2009.13539]](https://arxiv.org/pdf/2009.13539.pdf) The best place and time to live in the Milky Way

* until about six billion years ago, the outskirt of the Galaxy were safest places to live
* at the position of Earth, one LGRB may have occurred within the pasr 500 Myr.
* The safest zone in the past 500 Myr is within about 2-8 kpc
* Earth, from its birth until today, gradually becomes an increasingly safer place.


### [[2009.01253]](https://arxiv.org/pdf/2009.01253.pdf) Flavors of Astrophysical Neutrinos with Active-Sterile Mixing

* (3+1) flavor scenario, additional sterile state denoted as $\nu_s$ that is not produced in weak interactions, but that mixed with the active neutrinos.
* sterile neutrino production at the source can only be a result BSM physics, e.g. dark matter decy or annihilation
* in (3+1) scenario, U is 4X4 unitary mixing with 12 dof, 6 mixing angle, 6 physical phase
* if no $\nu_s$ are produced ar the astrophysical sources, the unitary boundary of allowed flavor composition at Earth in the 3+1 scenario is only marginally extended compared to the three flavor mixing. 


### [[2009.02195]](https://arxiv.org/pdf/2009.02195.pdf) Flavor ratios of astrophysical neutrinos interacting with stochastic gravitational waves having arbitrary spectra

* accounting for the interaction with stochastic GWs and averaging over the positions of randomly distributed neutrino sources, the fluxes at a detector are not (1:1:1)



## 2020 November

### [[2011.10933]](https://arxiv.org/pdf/2011.10933.pdf) Flavor Triangle of the Diffuse Supernova Neutrino Background


### [[2011.09869]](https://arxiv.org/pdf/2011.09869.pdf) Modeling of the Tau and Muon Neutrino-induced Optical Cherenkov Signals from Upward-moving Extensive Air Showers



## 2020 December

### [[2012.12893]](https://arxiv.org/pdf/2012.12893.pdf) The Future of High-Energy Astrophysical Neutrino Flavor Measurements

* future neutrino telescopes may be able to use timing information to distinguish $\nu_{e}$-induced electromagnatic showers from hadronic showers originating mainly from $\nu_{\tau}$'s, by using the difference in their late-time Cherenkov echoes from low-energy muons and neutrinos.
* requires a low level delayed pulses that could mimic muon and neutrino echoes.


### [[2012.12870]](https://arxiv.org/pdf/2012.12870.pdf) Lunar neutrinos

* cosmic rays scatters off regolith $\rightarrow$ slows down the mesons $\rightarrow$ large neutrino flux
* Moon, a source of neutrinos in sub-GeV energy range 
* a suppression of lunar neutrino flux compared to atmospheric by a factor of $10^{-2}-10^{-4}$
* both lunar and atmosphetic neutrinos have the same origin in cosmic rays
* although the ratio of total lunar to atmosphetic neutrino fluxes is close to unity, their spectra are quite dsifferent
* makes it potentially to distinguish between neutrinos of different origin, in spite of small Moon's solid angle vs whole sky $(\sim 5 \times 10^{-6})$
* Absence of atmosphere makes the Moon a very effective cosmic ray dump
* lunar soil density $\rho \approx 1.5 g/cm^{3}$
* most $\pi^{-}$ et captured bt nuclei via Voulomb attraction
* stopped $\pi^{+}$ produce monochromatic line of $\nu_{\mu}$ at $E_{\mu} \approx 29.8$ MeV
* Kaons produce monochromatic line of $\nu_{\mu}$ at $E_{\mu} \approx 235.6$ MeV




## 2021 April

### [[2104.00459]](https://arxiv.org/pdf/2104.00459.pdf) The Radar Echo Telescope for Cosmic Rays: Pathfinder Experiment for a Next-Generation Neutrino Observatory

* radar echo method: sensitive to neutrinos in the 10-100 PeV range
* high energy neutrino interact in a dense medium creates a cascade of relativistic particles that ionize atoms in the target medium. A short-lived cloud of charge is left behind, which can, if sufficiently dense, reflect incident radio waves. 



## 2021 May

### [[2105.03272]](https://arxiv.org/pdf/2105.03272.pdf) On the Tau flavor of the cosmic neutrino flux

* $\tau$ flavor holds a special place among the neutrino flavors in elucidating new physics
* how the detection of two tau neutrinos in IceCube constrains the interaction of neutrinos with ultralight dark matter
* at energies of EeV, Earth becomes opaque for the active neutrinos, active components of mass eigenstates become absorbed.
* high energy $\nu_4$ crossing the Earth could not produce $\tau$ events detected by ANITA
* 


### [[2105.03792]](https://arxiv.org/pdf/2105.03792.pdf) The IceCube Pie Chart: Relative Source Contributions to the Cosmic Neutrino Flux

* pie chat shows fractional contribution of each source type to IceCube's total neutrino flux
* blazars contribute $\leq 11\%$
* AGNs and TDEs contribute > $50\%$
* unkown source types contribute $\geq 10\%$



## 2021 June
 
### [[2106.08339]](https://arxiv.org/pdf/2106.08339.pdf) Neutrino As the Dark Force

* selt-interaction dark matter can occur without introducing dark force carries, but rather via the exchange of SM neutrinos.
* generate a $1/r^2$ potential between dark matter at distances shorter than the inverse of the neutrino mass
* a parameter space where dark matter has sufficiently strong self interaction to influence small-scale structure formation.
* the interaction between dark matter and neutrinos could accommodate a warm dark matter candidate.


## 2021 July

### [[2107.00532]](https://arxiv.org/pdf/2107.00532.pdf) The unfinished fabric of the three neutrino paradigm



### [[2107.01159]](https://arxiv.org/pdf/2107.01159.pdf) Neutrino constraints on long-lived heavy dark sector particle decays in the Earth

* a scenario with long lived ($\tau_{\chi}\sim 10^{28}$s), super heavy ($m_{\chi}\sim 10^{7}- 10^{10}$ GeV) dark matter 
* decay via $\chi \rightarrow \nu_{\tau}H$ or $\chi \rightarrow \nu_{\mu}H$



### [[2107.02604]](https://arxiv.org/pdf/2107.02604.pdf) Reconstructing the neutrino energy for in-ice radio detectors

* RNO-G search for astrophysical neutrinos at energies > 10 PeV
* high energy particle showers in cold ice emit radio signals that are hardly scattered and show attenuation lengths around 1 km.
* in practical terms, the event reconstruction of a radio-based neutrino detector can be thought of as the reconstruction of a particle cascade.
* a particle shows develops in the ice, positrons in the shower front annihilate with $e^-$ in the ice. Besides, $e^-$ in the ice are kicked out of their atomic shells and swept along in the shower. Both effects cause an excess of negetive charged to develop in the shower, which emits radio signals as they propagate.
* Such process happens in dense media, including ice, particle accelerators, air showers.
* shower energy resolution $\sim 30\%$


### [[2107.08063]](https://arxiv.org/pdf/2107.08063.pdf) Studying Bioluminescence Flashes with the ANTARES Deep Sea Neutrino Telescope

* almost constant background for each optical module with photon count rates around 40-60 kHz
* the posterior approximation is performed by the MGVI algorithm


### [[2107.12086]](https://arxiv.org/abs/2107.12086) Vacuum or matter symmetries: Which is fundamental in neutrino oscillation?




### [[2107.11573]](https://arxiv.org/abs/2107.11573) The Data-Directed Paradigm for BSM searches


## 2021 August

### [[2108.07881]](https://arxiv.org/abs/2108.07881) Can a strong radio burst escape the magnetosphere of a magnetar?

* a magnetar is a type of neutron star, powerful magnetic field ($\sim 10^9 to 10^11$ T)
* FRBs are emitted from the magnetosphere of a magnetar
* radio wave interact with low density $e^{\pm}$ plasma in the outer magnetosphere at radii $10^9 - 10^10$ cm with huge cross section for scattering the wave
* Possible emission scenarios: (1) emission near the magnetar, inside its ultrastrong magnetosphere ("near-filed"); (2) emission at much larger radii from explosions launched by magnetospheric flares into the magnetar wind ("far-field")



### [[2108.06928]](https://arxiv.org/abs/2108.06928) Neutrino Interactions in the Late Universe



### [[2108.13412]](https://arxiv.org/abs/2108.13412) Using Secondary Tau Neutrinos to Probe Heavy Dark Matter Decays in Earth

* a full treatment of tau neutrino propagation and regeneration within the Earth, using NuTauSim
* photonuclear effect accounts for most energy loss above $\sim 10^8$ GeV



## 2021 September

### [[2109.02900]](https://arxiv.org/abs/2109.02900) Unstable Cosmic Neutrino Capture



## 2021 November

### [[2111.07712]](https://arxiv.org/pdf/2111.07712) The Crab Pulsar and Nebula as seen in gamma-rays
* Crab pulsar: first detected pulsar
* Crab pulsar, predicted source based on the need for an energy soutce to power the Crab nebula.


### [[2111.10169]](https://arxiv.org/abs/2111.10169) A search for neutrino emission from cores of Active Galactic Nuclei

* AGN, most powerful emitters of radiation, has the potential to accelerate protons up to the highest observed CR energies and are surrounded by high-intensity radiation fields where photo-nuclear reactions with subsequent neutrino production can occur.

* Seyfert 2 Galaxy NGC 1068 is the most significant point

* probe two scenarios of high-energy production in AGN cores: 1. the neutrino production is proportional to the accretion disk luminosity of the AGN; 2. CR acceleration is suppressed in the AGN with the highest luminosities.


### [[2111.13048]](https://arxiv.org/abs/2111.13048) Neutrino Tomography of the Earth with ORCA Detector

* PREM model, energy density distribution is spherically symmetric
* mean Earth radius: 6371 km
* core radius: 3480 km; IC:0-1221.5 km, OC: 1221.5-3480 km
* mean density of mantle: 4.45 $g/cm^3$
* mean density of core: 10.99 $g/cm^3$
* oscillation between $\nu_{e}$ and $\nu_{\mu}$ in the range $E \sim (0.1-15.0) GeV$
* atmospheric neutrinos: perfect tool for performing Earth tomography
  