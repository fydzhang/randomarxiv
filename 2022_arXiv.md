## 2022 March 

### [[2202.11120]](https://arxiv.org/abs/2202.11120) toise: a framework to describe the performance of high-energy neutrino detectors



## 2022 April

### [[2204.05060]](https://arxiv.org/abs/2204.05060) A multi-messenger study of the blazar PKS 0735+178: a new major neutrino source candidate
* PKS 0735+178 is a bright radio and 𝛾-ray blazar
* Blazers are sub-classified depedning on their optical spectrum and on their radio to X-ray Spectral Energy Distribution (SED):FSRQ (broad emission lines) nd BL Lacs (featureless optical spectrum or very weak emission lines)
* The IHBL object PKS 0735+178 is one of the brightest BL Lac objects in the sky.
* IC211208A, a track-like event with energy of 172 TeV
* one cascade Baikal neutrino with estimated energy og 43 TeV, detected 3.95 hours after IceCube event
* KM3NeT, on Dec 15, 2021, an additional neutrino with energy of 18 TeV and p-value of the association with PKS 0735+178 is 0.14
* PKS 0735+178 is located outside the 90% error resgion of IC211208A

## 2022 August

### [[2208.05457]](https://arxiv.org/abs/2208.05457) Evidence for PeV Proton Acceleration from Fermi-LAT Observations of SNR G106.3+2.7
* G106.3+2.7 is a conmet-shaped, middle-aged (~10 kyr) SNR at a distance of ~800 pc
* High-energy (0.1-100 GeV) gamma-ray are crucial to break the degeneracy of the hadronic and leptonic scenarios
* Very High Energy (0.1-100 TeV) gamma ray emission of the SNR appears to come from the tail
* 12 years of Fermi-LAT data
* In the 0.1-1GeV region, excess emission is presented in the entire vicinity of the remnant with low significance
* As the 68% containment radius of the PSF of the LAT below 1 GeV is larger than 2 deg, the photons may also come from nearby sources or Galactic plane.

 
### [[2208.08423]](https://arxiv.org/pdf/2208.08423) Galactic contribution to the high-energy neutrino flux found in track-like IceCube events
* $4.1~\sigma$ evidence for the existence of an anisotropic component of the neutrino flux above 200 TeV 


### [[2208.09532]](https://arxiv.org/abs/2208.09532) IceCube search for neutrinos coincident with gravitational wave events from LIGO/Virgo run O3
* low-latency follow-up searches and archival searches for high-energy neutrino emission from the GW events 
* No significant neutrino emission was observed


### [[2208.11072]](https://arxiv.org/abs/2208.11072) Evaluation of the potential of a gamma-ray observatory to detect astrophysical neutrinos through inclined showers
* identification via the measurement of ver y inclined extensive air showers induced by downward-going and upward-going neutrinos.
* discrimination of neutrino-induced showers in the overwhelming CR background by analyzing the balance of the total EM and muonic signals of the shower at the ground.
* restrict neutrinos 100 TeV - 100 PeV
* inclined EAS (zenith angle > 60 degree)
* proton-air interaction cross section is 7 orders of magnitude > neutrino-air
* proton-induced inclined shower cross a large amount of matter before reaching the ground level --> EM component gets absorbed, muons can reach the ground. --> called old shower
* neutrinos  can interact much closer to the detector stations, bothe EM and muonic components will be detected --> called young shower.
* balance between the amount of measured signal due to muons and electromagnetic particles --> used to discriminate neutrino from CR induced showers.
* key observables: total amount of signal produced by electronmagnetic particle (S_{em}) and by muons ($S_{\mu}$)
* aim: minimise the backgound so that neutrino candidate would be significant
* achieved with a Fisher linear discriminant analysis performed in the parameter space of $log_{10}(S_{\mu})$ and $log_{10}(S_{em})$


### [[2208.12274]](https://arxiv.org/pdf/2208.12274) Indication of a Local Source of Ultra-High-Energy Cosmic Rays in the Northern Hemisphere
* PAO and TA report significant differences in the observed energy spectra of UHECRs above 30 EeV
* the presence of a local astrophysical source in the Northern Hemisphere, which is only visible by TA, can reconcile PAO nad TA measurements up tp the highest energies.
* the presence of that local source is favored at the 5.6 sigma
* the data are best descibed by a source lying at a distance of about 14 Mpc


## 2022 September

### [[2209.03042]](https://arxiv.org/abs/2209.03042) Graph Neural Networks for Low-Energy Event Classification & Reconstruction in IceCube


### [[2209.06339]](https://arxiv.org/abs/2209.06339) New constraints on the dark matter-neutrino and dark matter-photon scattering cross sections from TXS 0506+056
* IC170922A, its gamma ray counterpart was identified with a flaaring blazer TXS0506+056, at a distance of 1421 Mpc
* consider the attenuation of the gamma ray and neutrino flux within the host galaxy
* the supermassive BH at the center of the blazar is expected to seed the formation of a dark matter spike that extends from $10^{-4}$ pc to 1 pc.
* impose that the attenuation due to dark matter-neutrino interactions is less than 10%
* find the upper limits $\frac{\sigma_{DM-\nu}}{m_{DM}} < 2.0 \times 10^{-29} cm^2/GeV$


### [[2209.08593]](https://arxiv.org/abs/2209.08593) Testing hadronic and photo-hadronic interactions as responsible for UHECR and neutrino fluxes from Starburst Galaxies
* recent Pierre Auger study: the correlation between UHECRs at the highest energies and source catalogues is explored.
* strong correlation (4.2 sigma, foreseen 5 sigma in 2026) between the arrival directions of UHECRs and the coordinates of the starburst galaxies (SBGs).
* modified version of Monte Carlo code $\textit{SimProp}$, hadronic processes in the environment pf sources are implemented for the first time
  

### [[2209.08068]](https://arxiv.org/abs/2209.08068) Constraints on the hosts of UHECR accelerators


### [[2209.10011]](https://arxiv.org/abs/2209.10011) Prospects for detection of a Galactic diffuse neutrino flux
* a diffuse $\gamma$ ray at ~PeV is likely due to several tens PeV CRs injected by galactic PeV accelerators (PeVatons)
* whether the knee in the CR spectrum is due to a change in the CR acceleration mechanism or to a transport effect is matter of debate.
* The $\gamma$-Optimized Model
* main consequences of $\gamma$-optimized model is that the hadronic gamma-ray emission dominates over the leptonic one
* placed upper limits on diffuse neutrino emission taking advantage of the template fitting analysis method, with the model templates accurately reproducing the spatial distribution of the expected Galactic emission 


### [[2209.13462]](https://arxiv.org/abs/2209.13462) Search for Gamma-Ray and Neutrino Coincidences Using HAWC and ANTARES Data
* data collected from 2015.07 - 2020.02, a live timeof 4.39 years
* found 3 coincidence event, with an estimated false-alarm rate of < 1 coincidence per year
* AMON analyses

### [[2209.13726]](https://arxiv.org/abs/2209.13726) Could quantum gravity slow down neutrinos?
* hunt for GRB neutrinos could be significant in quantum gravity research
* from previous study, noticeable feature that quantum spacetime would slow down some of the neutrinos while others would be sped up
* find, no evidence for neutrinos sped up by quantum spacetime properties, whereas the evidence for neutrinos slowed down is even stronger than previous found.

### [[2209.14710]](https://arxiv.org/abs/2209.14710) Letter of Interest: Ocean science with the Pacific Ocean Neutrino Experiment
* P-ONE Interdisciplinary Meeting
* P-ONE detector will be composed of several moored observatories (MOs) arranged in clusters
* One cluster will comprise 10 individual MOs
* each an instrumented mooring line of kilometer length, strating on the sea floor at 266m depth.
* Each MOs will host 20 instruments fro collecting light
* Interdisciplinary science
* Bioluminescence
* Acounstic tomography and monitoring
* Deep ocean dynamics and thermodynamics 



## 2022 October

### [[2210.01650]](https://arxiv.org/abs/2210.01650) High-energy neutrino-induced cascade from the direction of the flaring radio blazar TXS 0506+056 observed by the Baikal GVD in 2021
* TXS 9596+056 was experiencing a major flare across the entire electromagnetic spectrum, from radio to gamma rays.
* Optical properties of Baikal deep water,abs = (21-23), sca = (60-80) at wl = (480-500)
* Baikal-GVD data: 2018.04 - 2022.03, $3.5 \times 10^{10}$ events collected by the basic trigger
* after noise hit suppression, 14328 cscades with reconstructed energy $E> 10~\text{TeV}$ and OM hit multiplicity $N > 11$
* selecting only upward moving cascades $\cos \theta < -0.25$, a total of 11 found, 2.7 from atmospheric neutrinos, 0.5 from mis-reconstructed atmospheric muons
* GVD201418CA, 224 +/- 75 TeV, RA = 82.4, Dec = 7.1
* signalness of GVD201418CA cascade is 97.1% (signalness of track event IC17092A is 56.5%)


### [[2210.00202]](https://arxiv.org/abs/2210.00202) Neurino Cadence of TXS 0506+056 Consistent with Supermassive Binary Origin
* 2022.09.18 $\sim 170$ TeV neutrino alert coincidence with blazar TXS 0506+056
* show that the neutrino cadence of TXS 0506+056 is consistent with a SMBBH
* conclude the mass ratio of the two black holes must fulfill $q <0.3$ for masses $M_{tot} > 3 \dot 10^{8} M_{\cdot}$
* prediction with model: a flare existing in IceCube data during 2019.08 - 2021.01
* next flare should peak in time period 2023.01 - 2026.08 


### [[2210.03088]](https://arxiv.org/abs/2210.03088) High-energy neutrinos from choked-jet supernovae: searches and implications
* Choked-jet supernovae (cjSNe), supernovae powered by relativistic jets stalled in stellar materials, may lead to neutrino emission via photohadronic interactions while the coproduced gamma rays are absorbed.
* use 10 years of IceCube neutrino data
* search for a statistical correlation between neutrinos and SNe Ib/c (cjSNe can in principle be observed as SNe Ib/c, where progenitor stars are more massive and typically enclosed by denser extended materials)


### [[2210.04653]](https://arxiv.org/abs/2210.04653) Rejecting noise in Baikal-GVD data with neural networks
* 99% signal purity and 98% survival efficiency
* noise hit rate 20 - 100 kHz
* trigger condition: two adjacent OMs, within 100 ns time windows, signals are at least 4.5 and 1.5 p.e. If this condition is fulfilled, the data from all OMs exceeding the signal-level threshold (0.3 p.e.) is collected
* Each hit is characterized by: hit time, integral charge, maximal amplitude
* 5 parameters: integral charge, hit time, x,y,z coordinates 
* ResNet-like neural network


### [[2210.04930]](https://arxiv.org/abs/2210.04930) Constraints on populations of neutrino sources from searches in the directions of IceCube neutrino alerts
* "Gold" events: astrophysical signal purity > 50%
* "Bronze" events: astrophysical signal purity > 30%
* "signalness": ratio of expected number of events from signal to the expected total number of events
*  10 events per year in "Gold", 30 events in "Gold" and "Bronze"


### [[2210.05913]](https://arxiv.org/abs/2210.05913) Concept Study for Observing Galactic Neutrinos in Neptune’s Atmosphere
* SMBH at the center of our galaxy
* There are ways to bypass the dust and debris by using telescopes in the Infrared and Radio spectrum


### [[2210.06338]](https://arxiv.org/abs/2210.06338) Lorentz invariance violation induced threshold anomaly versus very-high energy cosmic photon emission from GRB 221009A
* GRB221009A redshift z=0.1505, photons observed by LHAASO, 18 TeV
* nontrival fact since extragalaxtic background light could absorb these photons severely and the flux is too weak to be observed
* gamma + gamma --> electron + electron. This annihilation process prohibits high energy photons from propagating a long distance in the Universe since they can react with background photons such as CMB and extragalactic background light (EBL)
* derive the threshold for above reactions: $E > E_{th} = \frac{m^2_e}{\epsilon_b}$
* For CMB photons, threshold is 411 TeV. CMB is almost transparent to this event
* For EBL photons, threshold is 261 GeV ~ 261 TeV. EBL photons do matter to cause an attenuation of cosmic photon propagtion
* $F_{abs} = F_{int} \times e^{- \tau}$, $\tau (E_{obs, z})$ is the optical depth
* 18 TeV photons, flux is suppressed by at least 10^{-8}
* a signal of deviation from the standard physics
* The consequence of LIV is the dispersion relation of photons
* the mean free path of 18 TeV photons is about z = 0.01 to 0.02, while GRB 221009 is 0.15


### [[2210.1137]](https://arxiv.org/abs/2210.11337) Extragalactic neutrino emission induced by Supermassive and Stellar Mass Black Hole mergers
* the detection of a third high-erngy neutrino from the direction of TXS0506+056 on Sep.18, 2022 by IceCube indicates that blazars are identified as high-energy neutrino sources.
* Starbust galaxies are believed to be sources of cosmic rays and high-energy neutrinos.
* A pure starbust constrution due to supernova remnants cannot explain the high-energy gamma-ray emission, but that the emission from the core could explain the MM signatures, including a possible neutrino signal.
* investigate whether mergers of supermassive BBHs and/or stellar mass BBHs could be dominant sources of diffuse astrophysical neutrino flux measured by the IceCube.
* Motivated by the UHECR anisotropy study by Auger. It was found 10% of arriving UHECRs could be clustered around nearby starburst galaxies (M82), the rest contributed by other AGNs.
